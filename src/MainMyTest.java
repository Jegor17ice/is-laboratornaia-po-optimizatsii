

import static org.junit.jupiter.api.Assertions.*;

class MainMyTest {
    /**
     *Тест отрицательного числа
     * @throws Exception
     */
    @org.junit.jupiter.api.Test
    void count() throws Exception {
        long n = -1984;
        long s = 0;
        assertEquals(0, MainMy.count(n, s));
    }
    /**
     *Тест переполнения
     * @throws Exception
     */
    @org.junit.jupiter.api.Test
    void count1() throws Exception {
        long n = 1234123984 * 12123213;
        long s = 0;
        assertEquals(0, MainMy.count(n, s));
    }
    /**
     *Тест нуля
     * @throws Exception
     */
    @org.junit.jupiter.api.Test
    void count2() throws Exception {
        int n = 0;
        int s = 0;
        assertEquals(0, MainMy.count(n, s));
    }
    /**
     *Тест числа с полностью положительными цифрами
     * @throws Exception
     */
    @org.junit.jupiter.api.Test
    void count3() throws Exception {
        int n = 2222;
        int s = 0;
        assertEquals(8, MainMy.count(n, s));
    }
    /**
     *Тест числа с нечетными числами
     * @throws Exception
     */
    @org.junit.jupiter.api.Test
    void count4() throws Exception {
        int n = 13579539;
        int s = 0;
        assertEquals(0, MainMy.count(n, s));
    }


}