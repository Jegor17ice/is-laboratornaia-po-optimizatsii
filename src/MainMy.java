import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Программма для подсчета четных цифр в введенном числе
 * @author Иванчин Егор 17ит18
 */
public class MainMy {

    public static void main(String[] args) {
        long a = System.currentTimeMillis();
        long n = scan();
            long s = 0;
            try {

                s = count(n, s);
            }catch (Exception e){
                System.out.println("Ошибка подсчета");
            }
            System.out.println("s = " + s);
        long b = System.currentTimeMillis();
        long finalTime = b - a;
        System.out.println("Время работы : " + finalTime);

    }
    public static long scan(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите число в котором хотите посчитать все четные");
        long n = 0;
        try {
            n = sc.nextLong();
        } catch (InputMismatchException e) {
            System.out.println("Ошибка, введите число не более чем 10^9");
        }
        return n;
    }

    /**
     * Подсчет четных цифр
     * @param n - введенное пользователем число
     * @param s - счетчик
     * @return - количество четных цифр
     */
    public static long count(long n, long s) throws Exception {
        try {

            while (n > 1) {
                System.out.println(" n in while ="+n);
                if (n % 2 == 0) {
                    s = s + (n % 10);
                    System.out.println(" s in if ="+s);
                }
                n = n / 10;
                System.out.println("n after if ="+n);
            }
        }catch (Exception e ){
            throw new Exception();
        }
        return s;
    }
}
