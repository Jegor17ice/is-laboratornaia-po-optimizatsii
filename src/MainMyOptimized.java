import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Программма для подсчета суммы четных цифр в введенном числе
 *
 * @author Иванчин Егор 17ит18
 */
public class MainMyOptimized implements ConnectionWithUser {

    public static void main(String[] args) {
        long a = System.currentTimeMillis();
        System.out.println(ENTER_VALUE);
        long enterValue = 0;
        try { enterValue = scan();
        } catch (InputMismatchException e) {
            System.out.println(EXEPTION_SCAN_VALUE);
        }
        long count = 0;
        try { count = calculate(enterValue);
        } catch (Exception e) {
            System.out.println(EXEPTION_CALCULATE);
        }
        System.out.println(ALL_OUT_WoRDS + count);
        long b = System.currentTimeMillis();
        long finalTime = b - a;
        System.out.println(TIME_WORK_PROGRAMM + finalTime);
    }

    /**
     * Сканирование введенного пользователем значения
     *
     * @return - значение введенное пользователем
     */
    private static long scan() {
        Scanner scanner = new Scanner(System.in);
        long value = 0;
        try {
            value = scanner.nextLong();
        } catch (InputMismatchException e) {
            throw new InputMismatchException();
        }
        return value;
    }

    /**
     * Сложение всех четных цифр
     *
     * @param userValue - введенное пользователем число
     * @return - количество четных цифр
     */
    private static long calculate(long userValue) throws Exception {
        long counter = 0;
        try {
            while (userValue > 1) {
                if (userValue % 2 == 0) { counter = counter + (userValue % 10); }
                userValue /= 10;
            }
        } catch (Exception e) {
            throw new Exception();
        }
        return counter;
    }
}
